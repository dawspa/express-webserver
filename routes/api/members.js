const express = require("express");
const router = express.Router();
const uuid = require("uuid");
const members = require("../../Members");

// rest api json and code
// get all members
router.get("/", (req, res) => {
  res.json(members);
});

// get single member
router.get("/:id", (req, res) => {
  const found = members.some(member => member.id === req.params.id);

  if (found) {
    res.json(members.filter(member => member.id === req.params.id));
  } else {
    res.status(400).json({ msg: `No member with the id of ${req.params.id}` });
  }
});

//Create members
router.post("/", (req, res) => {
  const newMember = {
    id: uuid.v4(),
    name: req.body.name,
    email: req.body.email,
    status: "active"
  };

  req
    .checkBody("name")
    .notEmpty()
    .withMessage("Name is required")
    .isAlpha()
    .withMessage("Only alphabetical characters allowed in name");
  req
    .checkBody("email")
    .notEmpty()
    .withMessage("E-mail is required")
    .isEmail()
    .withMessage("Enter correct e-mail address");

  var errorsPost = req.validationErrors();
  if (errorsPost) {
    req.session.errorsPost = errorsPost;
    req.session.success = false;
    res.redirect("/#post");
  } else {
    req.session.success = true;
    members.push(newMember);
    res.redirect("/#get");
  }

  // if (!newMember.name || !newMember.email) {
  //   return res.status(400).json({ msg: "Please include a name and email" });
  // }
  // res.json(members);
});

//update member
router.put("/:id", (req, res) => {
  const found = members.some(member => member.id === req.params.id);
  if (found) {
    if (req.body.name || req.body.email) {
      req
        .checkBody("name")
        .optional({checkFalsy: true})
        .isAlpha()
        .withMessage("Only alphabetical characters allowed in name");
      req
        .checkBody("email")
        .optional({checkFalsy: true})
        .isEmail()
        .withMessage("Enter correct e-mail address");

      var errorsPut = req.validationErrors();
      if (errorsPut) {
        req.session.errorsPut = errorsPut;
        req.session.success = false;
        res.redirect("/#put");
      } else {
        req.session.success = true;
        const updMember = req.body;
        members.forEach(member => {
          if (member.id === req.params.id) {
            member.name = updMember.name ? updMember.name : member.name;
            member.email = updMember.email ? updMember.email : member.email;
          }
        });
        res.redirect("/#get");
      }
    } else {
      var errorsPut = [{msg: 'Please provide name or e-mail'}];
      req.session.errorsPut = errorsPut;
      req.session.success = false;
      res.redirect("/#put");
    }
  } else {
    var errorsPut = [{msg: 'User not selected'}];
    req.session.errorsPut = errorsPut;
    req.session.success = false;
    res.redirect("/#put");
  }
});

//delete member
router.delete("/:id", (req, res) => {
  const found = members.some(member => member.id === req.params.id);
  if (found) {
    const foundIndex = members.findIndex(member => member.id === req.params.id);
    members.splice(foundIndex, 1);
    req.session.success = true;
    res.redirect("/#get");
  } else {
    var errorsDel = [{msg: 'User not selected'}];
    req.session.errorsDel = errorsDel;
    req.session.success = false;
    res.redirect("/#delete");
  }
});

module.exports = router;
