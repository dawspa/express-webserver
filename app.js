const express = require("express");
const path = require("path");
// const logger = require("./middleware/logger");
const exphbs = require("express-handlebars");
const app = express();
const members = require("./Members");
const bodyParser = require("body-parser");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const expressValidator = require("express-validator");
const methodOverride = require('method-override');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(session({ secret: "dejw", saveUninitialized: false, resave: false }));
app.use(cookieParser());
app.use(methodOverride('_method'));

//init middleware
// app.use(logger);

//init session
app.use(session({ secret: "dejw", saveUninitialized: false, resave: false }));

// set static folder
app.use(express.static(path.join(__dirname, "public")));

//handlebars middleware
app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("view engine", "handlebars");

// body parser middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// set api route
app.use("/api/members", require("./routes/api/members.js"));

//homepage route
app.get("/", (req, res) => {
  res.render("index", {
    members,
    about: "about.html",
    contact: "contact.html",
    success: req.session.success,
    errorsPost: req.session.errorsPost,
    errorsPut: req.session.errorsPut,
    errorsDel: req.session.errorsDel,
    class: "show"
  });
  req.session.errorsPost = null;
  req.session.errorsPut = null;
  req.session.errorsDel = null;
});

//handle errors
app.use((req, res, next) => {
  res.status(404).render("404", {
    home: "/"
  });
});
app.use((err, req, res, next) => {
  console.error(err.stack);
  res
    .status(500)
    .send("<h1>Something broke!</h1><h2>Please contact administrator</h2>");
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
